package com.ucong.springangularapp.controller;

import com.ucong.springangularapp.dto.CustomerDetailsDTO;
import com.ucong.springangularapp.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CustomerController {
    /**
     * Handle to the service.
     */
    @Autowired
    private CustomerService customerService;

    /**
     * Gets all the customers and their orders.
     *
     * @return List<CustomerDetailsDTO>
     */
    @RequestMapping(value = "/customers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CustomerDetailsDTO> getCustomers() throws Exception {
        return customerService.getCustomersAndOrderData();
    }
}