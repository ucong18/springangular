package com.ucong.springangularapp.service;

import com.ucong.springangularapp.dto.CustomerDetailsDTO;

import java.util.List;

/**
 * Service interface that masks the caller from the implementation that fetches / acts on CustomerDetailsDTO
 * related data.
 *
 * @author ucong
 */
public interface CustomerService {

    /**
     * Gets all the customers and their orders.
     *
     * @return List<CustomerDetailsDTO>
     */
    List<CustomerDetailsDTO> getCustomersAndOrderData() throws Exception;
}
