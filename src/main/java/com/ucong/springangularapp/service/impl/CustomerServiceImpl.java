package com.ucong.springangularapp.service.impl;

import com.ucong.springangularapp.dao.CustomerRepository;
import com.ucong.springangularapp.dto.CustomerDetailsDTO;
import com.ucong.springangularapp.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    /**
     * Handle to the Data Access Layer.
     */
    @Autowired
    private CustomerRepository customerRepository;

    /**
     * Gets all the customers and their orders.
     *
     * @return CustomerDetailsDTO
     */
    @Override
    public List<CustomerDetailsDTO> getCustomersAndOrderData() throws Exception {
        return customerRepository.getCustomerDetails();
    }
}
